variable "cloud_id" {
  type    = string
  default = null
}

variable "service_acc_bindings" {
  description = <<EOT
    Example:

    service_acc_bindings = [
      {
        folder_name = "default"
        bindings = [
          {
            acc_name = "acc-1"
            folder_roles = [
              {
                folder = "default",
                roles  = ["admin"]
              },
            ],
            {
              folder = "dev",
              roles  = ["editor"]
            },
          },
        ]
      },
      {
        folder_name = "dev"
        bindings = [
          {
            acc_name = "acc-2"
            cloud_roles = ["editor"]
          },
        ]
      },
    ]
  EOT
  type = list(object({
    folder_name = string
    bindings = list(object({
      acc_name = string
      folder_roles = optional(list(object({
        folder = string
        roles  = list(string)
      })), [])
      cloud_roles = optional(list(string), [])
    }))
  }))
  default = []
  validation {
    condition = alltrue(
      [
        for entry in var.service_acc_bindings :
        length(
          [
            for other in var.service_acc_bindings :
            other.folder_name == entry.folder_name if other.folder_name == entry.folder_name
          ]
        ) == 1
      ]
    )
    error_message = "Duplicate folder_name values are not allowed."
  }
  validation {
    condition = alltrue(
      [
        for entry in var.service_acc_bindings :
        alltrue(
          [
            for binding in entry.bindings :
            length(
              [
                for other in entry.bindings :
                other.acc_name == binding.acc_name if other.acc_name == binding.acc_name
              ]
            ) == 1
          ]
        )
      ]
    )
    error_message = "Duplicate acc_name values within the same bindings array are not allowed."
  }
  validation {
    condition = alltrue(
      [
        for entry in var.service_acc_bindings :
        alltrue(
          [
            for binding in entry.bindings :
            alltrue(
              [
                for folder_role in binding.folder_roles :
                length(
                  [
                    for other in binding.folder_roles :
                    other.folder == folder_role.folder if other.folder == folder_role.folder
                  ]
                ) == 1
              ]
            )
          ]
        )
      ]
    )
    error_message = "Duplicate folder values within the same folder_roles array are not allowed."
  }
}
