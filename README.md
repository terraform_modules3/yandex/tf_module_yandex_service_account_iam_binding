# Yandex Cloud Service Account IAM Binding Terraform Module
Allows to create service account binding in Yandex cloud

## Usage example
```hcl
module "iam_bindings" {
  source = "git::https://gitlab.com/terraform_modules3/yandex/tf_module_yandex_users_iam_binding.git?ref=0.1.0"

  service_acc_bindings = [
    {
      folder_name = "default"
      bindings = [
        {
          acc_name = "acc-1"
          folder_roles = [
            {
              folder = "default",
              roles  = ["admin"]
            },
          ],
          {
            folder = "dev",
            roles  = ["editor"]
          },
        },
      ]
    },
    {
      folder_name = "dev"
      bindings = [
        {
          acc_name = "acc-2"
          cloud_roles = ["editor"]
        },
      ]
    },
  ]
}
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_yandex"></a> [yandex](#requirement\_yandex) | > 0.90 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_yandex"></a> [yandex](#provider\_yandex) | 0.113.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [yandex_resourcemanager_cloud_iam_member.acc_binding](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_cloud_iam_member) | resource |
| [yandex_resourcemanager_folder_iam_member.acc_binding](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/resourcemanager_folder_iam_member) | resource |
| [yandex_client_config.client](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/data-sources/client_config) | data source |
| [yandex_iam_service_account.acc](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/data-sources/iam_service_account) | data source |
| [yandex_resourcemanager_folder.acc_folder](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/data-sources/resourcemanager_folder) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloud_id"></a> [cloud\_id](#input\_cloud\_id) | n/a | `string` | `null` | no |
| <a name="input_service_acc_bindings"></a> [service\_acc\_bindings](#input\_service\_acc\_bindings) | Example:<br><br>    service\_acc\_bindings = [<br>      {<br>        folder\_name = "default"<br>        bindings = [<br>          {<br>            acc\_name = "acc-1"<br>            folder\_roles = [<br>              {<br>                folder = "default",<br>                roles  = ["admin"]<br>              },<br>            ],<br>            {<br>              folder = "dev",<br>              roles  = ["editor"]<br>            },<br>          },<br>        ]<br>      },<br>      {<br>        folder\_name = "dev"<br>        bindings = [<br>          {<br>            acc\_name = "acc-2"<br>            cloud\_roles = ["editor"]<br>          },<br>        ]<br>      },<br>    ] | <pre>list(object({<br>    folder_name = string<br>    bindings = list(object({<br>      acc_name = string<br>      folder_roles = optional(list(object({<br>        folder = string<br>        roles  = list(string)<br>      })), [])<br>      cloud_roles = optional(list(string), [])<br>    }))<br>  }))</pre> | `[]` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
