
data "yandex_client_config" "client" {}

data "yandex_resourcemanager_folder" "acc_folder" {
  for_each = { for folder in local.acc_folder_names : folder => folder }
  name     = each.key
  cloud_id = local.cloud_id
}

data "yandex_iam_service_account" "acc" {
  for_each  = { for folder_acc in local.folder_accs : folder_acc => folder_acc }
  folder_id = data.yandex_resourcemanager_folder.acc_folder[split(":", each.key)[0]].id
  name      = split(":", each.key)[1]
}
