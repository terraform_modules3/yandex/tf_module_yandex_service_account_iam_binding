

locals {
  cloud_id = var.cloud_id == null ? data.yandex_client_config.client.cloud_id : var.cloud_id
}
locals {
  _acc_folder_names = flatten([
    for entry in var.service_acc_bindings :
    [
      entry.folder_name
    ]
  ])

  _acc_roles_folder_names = flatten([
    for entry in var.service_acc_bindings :
    [
      for binding in entry.bindings :
      [
        for role in binding.folder_roles : role.folder
      ]
    ]
  ])

  acc_folder_names = distinct(concat(local._acc_folder_names, local._acc_roles_folder_names))

  folder_accs = distinct(flatten([
    for entry in var.service_acc_bindings :
    [for binding in entry.bindings : "${entry.folder_name}:${binding.acc_name}"]
  ]))

  acc_folder_roles_map = { for item in
    flatten([
      for folder in var.service_acc_bindings :
      [
        for binding in folder.bindings :
        [
          for folder_role_list in binding.folder_roles :
          [
            for role in folder_role_list.roles :
            {
              key   = "${folder.folder_name}:${binding.acc_name}:${folder_role_list.folder}:${role}"
              value = role
            }
          ]
        ]

      ]
    ])
    : item.key => item.value
  }

  acc_cloud_roles_map = { for item in
    flatten([
      for folder in var.service_acc_bindings :
      [
        for binding in folder.bindings :
        [
          for cloud_role in binding.cloud_roles :
          {
            key   = "${folder.folder_name}:${binding.acc_name}:${cloud_role}"
            value = cloud_role
          }
        ]

      ]
    ])
    : item.key => item.value
  }
}

resource "yandex_resourcemanager_folder_iam_member" "acc_binding" {
  for_each = local.acc_folder_roles_map

  folder_id = data.yandex_resourcemanager_folder.acc_folder[split(":", each.key)[2]].id
  role      = each.value
  member    = "serviceAccount:${data.yandex_iam_service_account.acc[join(":", slice(split(":", each.key), 0, 2))].id}"
}

resource "yandex_resourcemanager_cloud_iam_member" "acc_binding" {
  for_each = local.acc_cloud_roles_map

  cloud_id = local.cloud_id
  role     = each.value
  member   = "serviceAccount:${data.yandex_iam_service_account.acc[join(":", slice(split(":", each.key), 0, 2))].id}"
}
